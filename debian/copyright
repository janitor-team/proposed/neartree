Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: NearTree
Upstream-Contact: Herbert J. Bernstein
Source: The upstream sources obtained from https://github.com/yayahjb/neartree/
 have been modified by removing files which were unnecessary and of
 unknown copyright and licence status. See the get-orig-source target in
 debian/rules for exact list of removed files.

Files: *
Copyright: 2001, 2008, 2009, 2010, 2011, 2014, 2016 Larry Andrews
License: LGPL-2.1+

Files: debian/*
Copyright: 2009, 2011, 2013, 2018 Teemu Ikonen
License: LGPL-2.1+

Files: rhrand.h
Copyright: 2009 Rob Harrison, Larry Andrews, Herbert J. Bernstein
License: LGPL-2.1+

Files: triple.h
Copyright: 2010 Herbert J. Bernstein
License: LGPL-2.1+ and HP-notice

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
 MA  02110-1301  USA
 .
 On Debian systems the full text of the GNU Lesser General Public License,
 version 2.1 can be found in the `/usr/share/common-licenses/LGPL-2.1+'
 file.

License: HP-notice
 triple.h was derived from pair.h
 which was subject to the following notice:
 .
 Copyright (c) 1994
 Hewlett-Packard Company
 .
 Permission to use, copy, modify, distribute and sell this software
 and its documentation for any purpose is hereby granted without
 fee, provided that the above copyright notice appear in all copies
 and that both that copyright notice and this permission notice
 appear in supporting documentation.  Hewlett-Packard Company makes
 no representations about the suitability of this software for any
 purpose.  It is provided "as is" without express or implied
 warranty.
